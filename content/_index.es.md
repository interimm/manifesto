*El Espacio es el futuro de la humanidad.*

Es la oportunidad de la humanidad de explorar, desarrollar, usar y prosperar de manera diferente. Una manera de asegurar la *longevidad, sostenibilidad, apertura y equidad* de esos esfuerzos para toda la humanidad.

Para esto, nos comprometemos a cumplir lo siguiente:

Principios
----

1. Todas las personas deben tener el derecho a explorar y usar el espacio para el beneficio e interés de toda la humanidad.

2. La exploración y uso del espacio exterior a de llevarse a cabo colaborativa y cooperativamente.

3. El espacio exterior  debe de usarse exclusivamente  para propósitos pacíficos.

4. El beneficio económico no ha de ser el motor de la exploración espacial.

5. Todas las personas han de tener acceso al espacio exterior, tecnologías espaciales y los datos espaciales.

***

Para cumplir con estos principios, debemos lograr con lo siguiente:

Pilares
----

1.  **Código Abierto**

    Toda tecnología desarrollada para el espacio exterior ha de ser publicada y tener licencia de código abierto.

2.  **Datos Abiertos**

    Todos los datos relacionados con o producidos en el espacio exterior deben de ser de acceso libre, poder ser usados o basarse ellos por cualquier persona, en cualquier lugar, y han de ser compartidos y gestionadnos según los principios descritos anteriormente.

3.  **Desarrollo Abierto**

    Toda tecnología desarrollada para el espacio exterior ha de ser desarrollada de forma transparente, legible, documentada, comprobable, modular y de una manera eficiente.

4.  **Gobierno Abierto**

    Toda tecnología desarrollada para el espacio exterior ha de ser gobernada de una manera participativa, colaborativa, directa y de una manera distribuida.

***


Support
----

The Manifesto is a bedrock for development in the space age. By adding you or your organization, you can show your support for its principles and pillars.

To join as an organization or individual, sign below.

{{< form-sign action="https://airform.io/manifesto@libre.space" >}}